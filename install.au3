
func dialog($text)
	MsgBox(0, "Avram's Game Guru Toolkit", $text, 15)
endfunc

if ($CmdLine[0] > 0) then
	local $globallua = $CmdLine[1]
else
	local $globallua = "../global.lua"
endif

local $globFileExists = FileExists($globallua)

if (not $globFileExists) then
	dialog('The installer could not find your global.lua file. Try specifying path to global.lua in command line:' & @CRLF & @CRLF & 'install.exe "c:/path/to/global.lua"')
	exit 0
endif

local $globalluacontents = FileRead($globallua)
if (StringInStr($globalluacontents, 'require (aConfig.path .. "bootstrap")') > 0) then
	dialog("It seems Avram's Game Guru Toolkit is already installed in your global.lua file.")
	exit 0
else
	FileWriteLine($globallua, @CRLF & @CRLF)
	FileWriteLine($globallua, "-- Load Avram's Game Guru Toolkit")
	FileWriteLine($globallua, 'package.path = "scriptbank/?.lua;"')
	FileWriteLine($globallua, 'aConfig = aConfig or { path = "Avram/" }')
	FileWriteLine($globallua, 'require (aConfig.path .. "bootstrap")')

	$globalluacontents = FileRead($globallua)
	if (StringInStr($globalluacontents, 'require (aConfig.path .. "bootstrap")') > 0) then
		dialog("Avram's Game Guru Toolkit is successfully installed in your global.lua file.")
	else
		dialog("There was an error installing Avram's Game Guru Toolkit in your global.lua file!")
	endif
endif
