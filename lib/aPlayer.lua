--[[
Avram's Entitiy :: LUA class for FPSC:R
Author: Nemanja Avramovic
Version: 0.1 beta
URL: http://www.avramovic.info
--]]

aConfig = aConfig or nil

if type(aEntity) ~= 'table' then
  if aConfig == nil then
    require "Avram/lib/aEntity"
  else
    require (aConfig.path .. "lib/aEntity")
  end
end


aPlayerClass = aMisc.oop_extend(aEntity)

function aPlayerClass:exists()
  return true
end

function aPlayerClass:destroy()
  return not self:exists()
end

function aPlayerClass:_update()
  self.data = self.data or {}
  self.data["x"] = g_PlayerPosX
  self.data["y"] = g_PlayerPosY
  self.data["z"] = g_PlayerPosZ
  self.data["name"] = self.data["name"] or aConfig.playerName or "The Dude"
end

function aPlayerClass:attr(attr, val)
  local val = val or nil
  local attr = attr or nil

  self:_update()

  if not self:exists() then
    return nil
  end

  if attr == nil then
    return self.data
  else
    if val == nil then
      return self.data[attr]
    else
      self.data[attr] = val
      return self.data[attr] == val
    end -- of if val == nil
  end -- of if attr == nil
end

function aPlayerClass:holdingE()
  return self:holdingKey('e')
end

function aPlayerClass:pressedE()
  return self:pressedKey('e')
end

function aPlayerClass:holdingKey(key)
  local key = key:lower() or "e"
  local pressed = GetInKey()
  return key == pressed:lower()
end

function aPlayerClass:pressedKey(key)
  local key = key:lower() or "e"
  _aGlobals.waspressed = _aGlobals.waspressed or {}
  local waskeypressed = _aGlobals.waspressed[key] or false

  if not self:holdingKey(key) then
    _aGlobals.waspressed[key] = false
    return false
  else
    if not waskeypressed then
      _aGlobals.waspressed[key] = true
      return true
    else
      return false
    end
  end
end

function aPlayerClass:lastTyped(limit)
  _aGlobals.typed = _aGlobals.typed or {}
  local limit = limit or 20
  local pressed = GetInKey()
  local total = aMisc.count(_aGlobals.typed)
  
  if (total > limit*2) then
  	_aGlobals.typed = {}
  	total = 0
  end

  if _aGlobals.typed[total] ~= pressed then
  	_aGlobals.typed[total+1] = pressed
  end
  
  return table.concat(_aGlobals.typed, "")
end

function aPlayerClass:hasTyped(text, case_sensitive, limit)
  local limit = limit or 20
  local typed = self:lastTyped(limit)
  local case = case_sensitive or false
  if not case then
  	typed = typed:lower()
  	text = text:lower()
  end

  return (typed:find(text) ~= nil)

end


-- this function has been written by user smallg on the Game Guru forums
function aPlayerClass:isLookingAt(entity, distance, angle)
  if entity == nil then
  	return false
  end

  local distance = distance or 300
  local angle = angle or 0.5

  if self:distance(entity) <= distance then
    local destx = entity:attr('x') - g_PlayerPosX
    local destz = entity:attr('z') - g_PlayerPosZ
    local ang = math.atan2(destx, destz)
    ang = ang * (180.0 / math.pi)
    if ang < 0 then
      ang = 360 + ang
    elseif ang > 360 then
      ang = ang - 360
    end

    if g_PlayerAngY < 0 then
      g_PlayerAngY = 360 + g_PlayerAngY
    elseif g_PlayerAngY > 360 then
      g_PlayerAngY = g_PlayerAngY - 360
    end

    if g_PlayerAngY - angle <= ang and g_PlayerAngY + angle >= ang then
      return true
    else
      return false
    end
  else
    return false
  end

end

aPlayer = aPlayerClass:new()
aPlayer.e = 0