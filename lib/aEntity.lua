--[[
Avram's Entitiy :: LUA class for FPSC:R
Author: Nemanja Avramovic
Version: 0.1 beta
URL: http://www.avramovic.info
--]]

aConfig = aConfig or nil

if type(aMisc) ~= 'table' then
  if aConfig == nil then
    require "Avram/lib/aMisc"
  else
    require (aConfig.path .. "lib/aMisc")
  end
end

aEntity = {}
aEntity.__index = aEntity

function aEntity:new(e, name)
  local self = setmetatable({}, aEntity)

  local name = name or nil
  local e = e or nil

  if e == nil then
    return nil
  end

  if type(e) == 'number' then
    self.e = e or 0
    if name ~= nil and aMisc.empty(g_Entity[self.e]['name']) then
      g_Entity[self.e]['name'] = name
    end
  else
    return aEntity.by_name(e)
  end



  return self
end

function aEntity:exists()
  return self.e > 0 and type(g_Entity[self.e]) ~= nil
end

function aEntity:attr(attr, val)
  local val = val or nil
  local attr = attr or nil

  if not self:exists() then
    return nil
  end

  if attr == nil then
    return g_Entity[self.e]
  else
    if val == nil then
      return g_Entity[self.e][attr]
    else
      g_Entity[self.e][attr] = val
      return g_Entity[self.e][attr] == val
    end -- of if val == nil
  end -- of if attr == nil
end

function aEntity:distance(entity, pretty, decimals)
  local entity = entity or false
  local decimals = decimals or 1
  local pretty = pretty or false

  if not entity then
    return 0
  end

  if type(entity) == 'number' then
    entity = aEntity:new(entity)
  elseif type(entity) == 'string' then
    entity = aEntity.by_name(entity)
  end

  local myX = self:attr('x')
  local myY = self:attr('y')
  local myZ = self:attr('z')

  local otherX = entity:attr('x')
  local otherY = entity:attr('y')
  local otherZ = entity:attr('z')

  local dist = aMisc.distance(myX, myY, myZ, otherX, otherY, otherZ)

  if not pretty then
    return dist
  else
    return aMisc.distance_pretty(dist, decimals)
  end
end

function aEntity._get_Es_by_name(name, pattern)
  local name = name or nil
  local pattern = pattern or false
  local set = {}
  for i, ent in pairs(g_Entity) do
    if name == nil then
      table.insert(set, i)
    else
      local e_name = g_Entity[i]['name'] or false
      if e_name ~= false then
        if pattern and string.match(e_name, name) ~= nil then
          table.insert(set, i)
        elseif not pattern and e_name == name then
          table.insert(set, i)
        end -- of if pattern and name match
      end -- of if e_name ~= false
    end -- of if not name
  end -- of for i

  return set
end

function aEntity.get_entities_by_name(name, pattern)
  local set = aEntity._get_Es_by_name(name, pattern)
  local eSet = {}

  if aMisc.count(set) <= 0 then
    return eSet
  end

  for i, e in pairs(set) do
    table.insert(eSet, aEntity:new(e))
  end

  return eSet
end

function aEntity.by_name(name, pattern)
  local set = aEntity.get_entities_by_name(name, pattern)

  if aMisc.count(set) > 0 then
    return set[1]
  else
    return nil
  end
end

function aEntity.fetch(e)
  if (type(e) == "number") then
    return aEntity:new(e)
  elseif type(e) == "string" then
    return aEntity.by_name(e)
  elseif type(e) == "table" then
    local eInd = e.e or false
    if eInd then
      return e
    else
      return nil
    end
  else
    return nil
  end
end

function aEntity:nearest_entities(name, pattern, max_distance, max_entities)
  local eSet = {}
  local max_distance = max_distance or 0
  local max_entities = max_entities or 0
  local set = aEntity.get_entities_by_name(name, pattern)

  -- sort by distance
  aEntity.compare_with = self
  table.sort(set, aEntity._sort_by_distance)
  aEntity.compare_with = nil

  for i, ent in pairs(set) do
    -- add entity only if not itself, max_distance is lower than it's distance (or is 0) and number of max_entities has not been reached (or is 0)
    if (ent.e ~= self.e) and (max_distance == 0 or self:distance(ent) <= max_distance) and (max_entities == 0 or aMisc.count(eSet) <= max_entities) then
      table.insert(eSet, ent)
    end
  end

  return eSet
end

function aEntity:nearest(name, pattern, max_distance)
  local set = self:nearest_entities(name, pattern, max_distance, 1)

  if aMisc.count(set) > 0 then
    return set[1]
  else
    return nil
  end
end

function aEntity._sort_by_distance(a, b)
  return aEntity.compare_with:distance(a) < aEntity.compare_with:distance(b)
end

function aEntity:watch_death()
  local ihealth = self:attr('_dw_health') or nil
  local health = g_Entity[self.e]['health']

  if ihealth == nil and health > 0 then
    ihealth = health
    self:attr('_dw_health', ihealth)
    SetEntityHealth(self.e, health*2)
    self:attr('health', health*2)
  end
end

function aEntity:is_dead(keep_alive)
  local ihealth = self:attr('_dw_health') or 0
  local health = g_Entity[self.e]['health']
  local keep_alive = keep_alive or false

  if health <= ihealth and health > 0 and ihealth > 0 then
    if keep_alive~=true then
      SetEntityHealth(self.e, 0)
      g_Entity[self.e]['health'] = 0
    end
    return true
  else
    return false
  end
end

-- FPSC:R functions encapsulation
function aEntity:destroy()
  Destroy(self.e)
  self.e = 0
  return not self:exists()
end

function aEntity:collisionOn()
 CollisionOn(self.e)
end

function aEntity:collisionOff()
 CollisionOff(self.e)
end

function aEntity:collected()
 Collected(self.e)
end

function aEntity:setAnimation()
 SetAnimation(self.e)
end

function aEntity:playAnimation()
 PlayAnimation(self.e)
end

function aEntity:addPlayerWeapon()
 AddPlayerWeapon(self.e)
end

function aEntity:addPlayerAmmo()
 AddPlayerAmmo(self.e)
end

function aEntity:addPlayerHealth()
 AddPlayerHealth(self.e)
end

function aEntity:winZone()
 WinZone(self.e)
end

function aEntity:checkpoint()
 Checkpoint(self.e)
end

function aEntity:playSound0()
 PlaySound0(self.e)
end

function aEntity:playSound1()
 PlaySound1(self.e)
end