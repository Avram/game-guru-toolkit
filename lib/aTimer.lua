--[[
Avram's Timer :: LUA class for FPSC:R
Author: Nemanja Avramovic
Version: 0.1 beta
URL: http://www.avramovic.info
--]]

aTimer = {}
aTimer.__index = aTimer

_aGlobals = _aGlobals or {}
_aGlobals.timers = _aGlobals.timers or {}

function aTimer:construct(e, countdown, autostart)
  local self = setmetatable({}, aTimer)

  self.countdown = countdown or nil
  self.autostart = autostart or false
  self.cnt = 0
  self.started = 0
  self.e = e

  if self.autostart then
    self.started = os.time()
    self.cnt = 1
  end

  return self
end

function aTimer:running()
  return self.started > 0
end

function aTimer:start(countdown)

  if self:running() then
    return false
  end

  countdown = countdown or nil
  if countdown ~= nil and self.countdown == nil then
    self.countdown = countdown
  end
  self.started = os.time()
  self.cnt = self.cnt + 1
  self.autostart = false

  if (self.e > 0) then
    _aGlobals.timers[self.e] = self
  end

  return true
end

function aTimer:reset(countdown)
  local cntdwn = self.countdown or nil
  self:stop()
  return self:start(countdown)
end

function aTimer:stop()
  if not self:running() then
    return false
  end

  local passed = self:passed()
  self.started = 0

  if (self.e > 0) then
    _aGlobals.timers[self.e] = self
  end

  return passed
end

function aTimer:passed()
  if self:running() then
    return os.time() - self.started
  else
    return 0
  end
end

function aTimer:greater(seconds)
  return self:passed() > seconds
end

function aTimer:lower(seconds)
  return self:passed() < seconds
end

function aTimer:between(min, max, inclusive)
  inclusive = inclusive or true

  if inclusive then
    min=min-1
    max=max+1
  end

  return self:greater(min) and self:lower(max)
end

function aTimer:left()
  if self.countdown == nil then
    return 0
  end

  local left = self.countdown - self:passed()
  if left < 0 then
    left = 0
  end
  return left
end

function aTimer:expired()
  if self.countdown ~= nil then
    return self:left() <= 0
  else
    return false
  end
end

function aTimer:new(e, countdown, autostart)
  if type(e) == 'table' then
    return aTimer.for_e(e.e, countdown, autostart)
  end

  local timer = _aGlobals.timers[e] or aTimer:construct(e, countdown, autostart)
  _aGlobals.timers[e] = timer
  return _aGlobals.timers[e]
end

function aTimer.for_e(e, countdown, autostart)
  return aTimer:new(e, countdown, autostart)
end