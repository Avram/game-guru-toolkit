--[[
Avram's Misc :: LUA class for FPSC:R
Author: Nemanja Avramovic
Version: 0.1 beta
URL: http://www.avramovic.info
--]]

aMisc = {}
aMisc.__index = aMisc

-- count real elements of a table
function aMisc.count(table)
  local cnt = 0
  if type(table) ~= "table" then
    return 0
  end

  for i in pairs(table) do
    cnt=cnt+1
  end
  return cnt
end

-- trim string
function aMisc.trim(str)
  return string.gsub(str, "^%s*(.-)%s*$", "%1")
end

-- check if parameter is empty (nil, false, empty string or empty table)
function aMisc.empty(what)
  if what == nil then
    return true
  elseif what == false then
    return true
  elseif type(what) == 'string' and aMisc.trim(what) == "" then
    return true
  elseif type(what) == 'table' and aMisc.count(what) == 0 then
    return true
  end

  return false
end

function aMisc.round(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

-- get distance between two points in 3d space
function aMisc.distance(x1, y1, z1, x2, y2, z2)
  local DX = x1 - x2
  local DY = y1 - y2
  local DZ = z1 - z2

  return math.sqrt(math.abs(DX*DX)+math.abs(DY*DY)+math.abs(DZ*DZ))
end

function aMisc.distance_pretty(distance, decimals, as_string)
  local decimals = decimals or 1
  local as_string = as_string or true
  local output = {}
  output["distance"] = distance
  output["cm"] = distance * 2.54;
  --get meters
  local dist_m = aMisc.round(output["cm"] / 100, decimals)
  output["m"] = dist_m
  output["string"] = dist_m .. " m"

  if dist_m > 1000 then
    local dist_km = aMisc.round(dist_m / 1000, decimals)
    output["km"] = dist_km
    output["string"] = dist_km .. " km"
  end

  if as_string then
    return output["string"]
  else
    return output
  end
end

function aMisc.weight_pretty(weight, decimals, as_string)
  local decimals = decimals or 1
  local as_string = as_string or true
  local output = {}
  output["weight"] = weight
  --get meters
  local dist_m = aMisc.round(weight, decimals)
  output["gr"] = dist_m
  output["string"] = dist_m .. " gr"

  if dist_m > 1000 then
    local dist_km = aMisc.round(dist_m / 1000, decimals)
    output["kg"] = dist_km
    output["string"] = dist_km .. " kg"
  end

  if as_string then
    return output["string"]
  else
    return output
  end
end


-- calculate modulo if you can't use operator %
function aMisc.modulo(a, b)
  return a - math.floor(a/b)*b
end

-- check if a is divisible by b (without remainder)
function aMisc.divisible_by(a, b)
  return aMisc.modulo(a, b) == 0
end

-- return string "yes" or "no" based on bool
function aMisc.yesno(bool)
  if bool then
    return 'yes'
  else
    return 'no'
  end
end

-- return string "true" or "false" based on bool
function aMisc.truefalse(bool)
  if bool then
    return 'true'
  else
    return 'false'
  end
end

-- replaces ternary operand
function aMisc.ifelse(condition, if_true, if_false)
  if condition then
    return if_true
  else
    return if_false
  end
end

-- object inheritance
function aMisc.oop_extend(baseClass)

    -- The following lines are equivalent to the SimpleClass example:

    -- Create the table and metatable representing the class.
    local new_class = {}
    local class_mt = { __index = new_class }

    -- Note that this function uses class_mt as an upvalue, so every instance
    -- of the class will share the same metatable.
    --
    function new_class:new()
        local newinst = {}
        setmetatable( newinst, class_mt )
        return newinst
    end

    -- The following is the key to implementing inheritance:

    -- The __index member of the new class's metatable references the
    -- base class.  This implies that all methods of the base class will
    -- be exposed to the sub-class, and that the sub-class can override
    -- any of these methods.
    --
    if baseClass then
        setmetatable( new_class, { __index = baseClass } )
    end

    return new_class
end

function aMisc.in_table(what, table)
  if what == nil or table == nil then
    return false
  end

  for i, j in pairs(table) do
    if j == what then
      return true
    end
  end

  return false
end

function aMisc.random_string(table)
  if table == nil or aMisc.count(table) < 1 then
    return '#[ empty or no table provided ]#'
  end

  local total = aMisc.count(table)
  local ind = math.random(1, total)

  return table[ind]
end

function aMisc.serialize(table)
  local string = '';
  if type(table) == "number" then
    return tostring(table)
  elseif type(table) == "nil" then
    return 'nil'
  elseif type(table) == "boolean" then
    return aMisc.truefalse(table)
  elseif type(table) == "string" then
    local str = string.gsub(table, ',', '[!#COMMA#!]')
    str = string.gsub(str, '=', '[!#EQUALS#!]')
    --str = string.gsub(str, '"', '[!#DBLQUOTE#!]')
    return string.format("%q", str)
  elseif type(table) == "table" then
    string = string .. "{"
    local nr = aMisc.count(table)
    local i = 0
    for k,v in pairs(table) do
      i=i+1
      --if type(k) == 'string' then
        string = string .. " " .. k .. " = "
      --end
      string = string .. aMisc.serialize(v)

      if (i < nr) then
        string = string .. ", "
      end
    end
    return string .. ("}")
  else
    return nil
  end

end

function aMisc.unserialize(str)
  local str = aMisc.trim(str)
  local tbl = {}

  if aMisc.is_numeric(str) then
    return tonumber(str)
  elseif string.sub(str, 1, 1) == '"' and string.sub(str, -1, -1) == '"' then
    str = string.sub(str, 2,-2)
    str = string.gsub(str, '[!#COMMA#!]', ',')
    str = string.gsub(str, '[!#EQUALS#!]', '=')
    --str = string.gsub(str, '[!#DBLQUOTE#!]', '"')
    return aMisc.trim(str)
  elseif string.lower(str) == 'nil' then
    return nil
  elseif string.lower(str) == 'true' then
    return true
  elseif string.lower(str) == 'false' then
    return false
  end

  -- strip out { and } from both ends
  if string.sub(str, 1, 1) == '{' and string.sub(str, -1, -1) == '}' then
    str = aMisc.trim(string.sub(str, 2, -2))
  end

  --get tables
  for kw, val in string.gmatch(str, '([a-zA-Z0-9]+)%s*=%s*(%b{})') do
    if aMisc.is_numeric(kw) then
      kw = tonumber(kw)
    end
    tbl[kw] = aMisc.unserialize(val)
    str = string.gsub(str, '([a-zA-Z0-9]+)%s*=%s*(%b{})%s*,?', '')
  end
  --get numbers
  for kw, val in string.gmatch(str, '([a-zA-Z0-9]+)%s*=%s*(%d+)') do
    if aMisc.is_numeric(kw) then
      kw = tonumber(kw)
    end
    tbl[kw] = aMisc.unserialize(val)
    str = string.gsub(str, '([a-zA-Z0-9]+)%s*=%s*(%d+)%s*,?', '')
  end
  --get rest (strings, booleans, nil)
  for kw, val in string.gmatch(str, '([a-zA-Z0-9]+)%s*=%s*(".-")') do
    if aMisc.is_numeric(kw) then
      kw = tonumber(kw)
    end
    tbl[kw] = aMisc.unserialize(val)
    str = string.gsub(str, '([a-zA-Z0-9]+)%s*=%s*(".-")%s*,?', '')
  end
  --get nils
  for kw, val in string.gmatch(str, '([a-zA-Z0-9]+)%s*=%s*(nil)') do
    if aMisc.is_numeric(kw) then
      kw = tonumber(kw)
    end
    tbl[kw] = nil
    str = string.gsub(str, '([a-zA-Z0-9]+)%s*=%s*(nil)%s*,?', '')
  end
  --get trues
  for kw, val in string.gmatch(str, '([a-zA-Z0-9]+)%s*=%s*(true)') do
    if aMisc.is_numeric(kw) then
      kw = tonumber(kw)
    end
    tbl[kw] = true
    str = string.gsub(str, '([a-zA-Z0-9]+)%s*=%s*(true)%s*,?', '')
  end
  --get falses
  for kw, val in string.gmatch(str, '([a-zA-Z0-9]+)%s*=%s*(false)') do
    if aMisc.is_numeric(kw) then
      kw = tonumber(kw)
    end
    tbl[kw] = false
    str = string.gsub(str, '([a-zA-Z0-9]+)%s*=%s*(false)%s*,?', '')
  end
  return tbl

end

function aMisc.is_numeric(what)
  return tonumber(what) ~= nil
end

function aMisc.explode(str, sep)
  local sep, fields = sep or ",", {}
  local pattern = string.format("([^%s]+)", sep)
  string.gsub(str, pattern, function(c) fields[aMisc.count(fields)+1] = c end)
  return fields
end

function aMisc.xorEncrypt(data, key)
    local klen = key:len()
    local s = ""
    for i = 1, data:len() do
        s = s..string.char(bit32.bxor(string.byte(data, i), string.byte(key, aMisc.modulo(i, klen))))
    end
    return s
end

function aMisc.str2tbl(str)
  local len = string.len(str)
  local ret = {}
  for i=1,len do
    table.insert(ret, string.sub(str, i, i))
  end
  return ret
end

function aMisc.urlencode(str)
  if (str) then
  str = string.gsub (str, "\n", "\r\n")
  str = string.gsub (str, "([^%w ])", function (c) return string.format ("%%%02X", string.byte(c)) end)
  str = string.gsub (str, " ", "+")
  end
  return str
end

-- encoding
function aMisc.base64_encode(data)
  local barr='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    return ((data:gsub('.', function(x) 
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (x:len() < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return barr:sub(c+1,c+1)
    end)..({ '', '==', '=' })[aMisc.modulo(data:len(), 3)+1])
end

function aMisc.base64_decode(data)
  local barr='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    data = string.gsub(data, '[^'..barr..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(barr:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (x:len() ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end