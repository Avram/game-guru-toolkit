--[[
Avram's Toolkit :: LUA class for FPSC:R
Author: Nemanja Avramovic
Version: 0.1 beta
URL: http://www.avramovic.info
--]]

aConfig = aConfig or {}

--aConfig.playerName = "The Dude"

aConfig.modules = aConfig.modules or {
    "aMisc"
    ,"aTimer"
    ,"aEntity"
    ,"aPlayer"
}

aConfig.randomize = true

aConfig.path = aConfig.path or "Avram/"