--[[
Avram's Toolkit :: LUA class for FPSC:R
Author: Nemanja Avramovic
Version: 0.1 beta
URL: http://www.avramovic.info
--]]

aConfig = aConfig or nil

if aConfig == nil then
  require "Avram/config"
elseif aConfig.path ~= nil then
  require (aConfig.path .. "config")
end

aGlobals = {}
_aGlobals = {}

if aConfig ~= nil and aConfig.modules ~= nil then
  for i, mod in pairs(aConfig.modules) do
    require (aConfig.path .. "lib/" .. mod)
  end
end

_aGlobals._randomize = aConfig.randomize or false

if _aGlobals._randomize then
	math.randomseed(os.time())
	math.random(); math.random(); math.random()
end

require (aConfig.path .. "helpers")