--[[
Put your helper functions in this file. Helper functions are simple functions which you'll often need in your game(s).

Below is a fictional player_info function that returns a string about a number of (filled) water bottles player
is carrying in a fictional survival game. Then you can just use Promp(player_info()) in your code.

function player_info()
	local water = aPlayer:attr('water') or 0
	local bottles = aPlayer:attr('bottles') or 0
	local cash = aPlayer:attr('cash') or 0

	return "WATER: "..water.."/"..bottles.."l; CASH: $"..cash
end

Place your code below this line: --]]

